<?php

/**
 * @file
 * Hooks provided by the jQuery browser language module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow modules to exclude the jQuery browser language functionality pr. page.
 *
 * @return bool
 *   True if the page being shown does not have to worry about language
 *   detection.
 */
function ida_language_jquery_browser_language_exclude_page() {
  $path = current_path();
  $parts = explode('/', $path);
  if (isset($parts[0]) && in_array($parts[0], array('some', 'unwanted', 'pages'))) {
    return TRUE;
  }
  return FALSE;
}

/**
 * @} End of "addtogroup hooks".
 */
