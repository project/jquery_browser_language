/**
 * Uses the user's browser language settings to redirect to the appropiate
 * language if found. Sets a cookie to remember.
 */
(function ($) {

Drupal.behaviors.jquery_browser_language = {
  attach: function (context, settings) {
    // Exclude page if there was a module requesting exclusion.
    if (Drupal.settings.jquery_browser_language.exclude_page) {
      return;
    }
    // Attach a listener that sets the cookie.
    $('.language-switcher-locale-url .language-link').click(function() {
      $.cookie('pref-browser-lang', this.getAttribute('lang').substring(0, 2), {path: '/'});
    });

    // Get the path prefix if any.
    var path_prefix = window.location.pathname.split('/');
    // Only redirect if the cookie is not set and the path prefix is not
    // correct.
    if ((Drupal.settings.jquery_browser_language.standard != $.cookie('pref-browser-lang')) && $.cookie('pref-browser-lang') != path_prefix[1]) {
      $.browserLanguage(function(language, acceptHeader) {
        var browserLang = acceptHeader.substring(0, 2);
        // If the browser language is not in the array of languages the site is
        // using, then don't do anything.
        if ($.inArray(browserLang, Drupal.settings.jquery_browser_language.lang_list) != -1) {
          if (path_prefix[1] != browserLang) {
            window.location.replace('/' + browserLang + window.location.pathname + location.search);
          }
        }
      });
    }
  }
};
})(jQuery);
