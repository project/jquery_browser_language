This module uses the language settings from the user's browser to redirect to the path prefix matching the language code if that langugage is enabled on the site. It all happens with javascript.

Let's say that I have my browser set up to use English as main language and I am visiting a site that has Danish as its default language. I would then be redirected from http://example.com to http://example.com/en every time I hit the url without the path prefix.

If you have a language switcher available to the users on the site (Drupal's own Language Switcher Block), then users preference will be saved in a cookie when one of the languages in the language chooser is clicked.

The module depends on the Libraries API module http://drupal.org/project/libraries and uses dansingerman's jQuery-Browser-Language script https://github.com/dansingerman/jQuery-Browser-Language

Installation:
* Download this zip: https://github.com/dansingerman/jQuery-Browser-Language/archive/master.zip and unzip it to your libraries folder (see http://drupal.org/node/1440066 ) and rename the unzipped folder to "jquery_browser_language".
* Download the module and the Libraries API and enable both modules.


Note that if you change path prefixes to something other than the two-letter codes like 'en', 'da' this will not work. The code naïvely takes the two first letters of the accept header from the browser and uses that as path prefix. If you use default settings you should be fine. The language codes conform to these standards: http://en.wikipedia.org/wiki/ISO_639-1_language_matrix

Module is developed at Reload! and sponsored by IDA.
